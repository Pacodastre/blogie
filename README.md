# Blogie

To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

## To add back later

```elixir
<.link
  href={~p"/users/log_out"}
  method="delete"
  class="text-[0.8125rem] leading-6 text-zinc-900 font-semibold hover:text-zinc-700"
>
  Log out
</.link>



<.link class="btn btn-secondary" href={~p"/admin/posts"}>Admin area</.link>

```


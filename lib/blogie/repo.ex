defmodule Blogie.Repo do
  use Ecto.Repo,
    otp_app: :blogie,
    adapter: Ecto.Adapters.Postgres
end

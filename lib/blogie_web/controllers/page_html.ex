defmodule BlogieWeb.PageHTML do
  use BlogieWeb, :html

  embed_templates "page_html/*"
end

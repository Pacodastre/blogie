defmodule BlogieWeb.Layouts do
  use BlogieWeb, :html

  embed_templates "layouts/*"
end
